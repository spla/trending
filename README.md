# Mastodon's trending manager

This code automatically approve hashtags and notify moderators via private mention about pending posts and links.

### Dependencies

-   **Python 3**
-   Mastodon bot account with the custom role (Manage Users & Manage Taxonomies)
-   Mastodon running server.

### Usage:

Within Python Virtual Environment:

1. Run `pip install -r requirements.txt` to install needed libraries.

2. Run `python trending.py`. First run will set everything up.

3. Use your favourite scheduling method to run `python trending.py` periodically.



