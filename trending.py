from app.libraries.setup import Setup
from app.libraries.moderators import Mods
from mastodon import Mastodon
import requests
import pdb

# main

def build_mods_str(dict):

    mods_str = ''
    for mod in dict.values():
        mods_str += f'@{mod} '

    return mods_str

if __name__ == '__main__':

    setup = Setup()

    mastodon = Mastodon(
        access_token = setup.mastodon_app_token,
        api_base_url= setup.mastodon_hostname
    )

    mods = Mods(mastodon=mastodon)

    mods_str = build_mods_str(mods.list)

    for status in mastodon.admin_trending_statuses():

        if status.language == 'ca' and status.requires_review:

            post_text = f"{mods_str}:\n\nhi ha tuts pendents de validar\n\nhttps://mastodont.cat/admin/trends/statuses?locale=ca"

            mastodon.status_post(post_text, visibility="direct")

    for link in mastodon.admin_trending_links():

        if link.language == 'ca' and link.requires_review:

            post_text = f"{mods_str}:\n\nhi ha enllaços pendents de validar\n\nhttps://mastodont.cat/admin/trends/links?locale=ca"

            mastodon.status_post(post_text, visibility="direct")

    for tag in mastodon.admin_trending_tags():

        if tag.requires_review:

            headers = {
                    "Authorization": f"Bearer {setup.mastodon_app_token}"
                    }

            endpoint = 'https://' + setup.mastodon_hostname + '/api/v1/admin/trends/tags/{0}/approve'.format(tag.id)

            try:

                response = requests.post(endpoint, headers=headers)

                if response.ok:

                    post_text = f"{mods_str}:\n\netiqueta #{tag.name} aprovada"

                    mastodon.status_post(post_text, visibility="direct")

            except:

                pass

