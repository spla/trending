import os
import sys
import pickle
from mastodon.Mastodon import MastodonUnauthorizedError
import pdb

class Mods():

    name = "Mods"

    def __init__(self, config_file=None, mastodon=None, list={}):

        self.config_file = "config/mods.txt"

        self.mastodon = mastodon

        self.list = list

        try:

            self.mastodon.me()

        except AttributeError as e:

            print(f"\n{e}, missing mastodon object\n")

        except MastodonUnauthorizedError as mue:

            print(f"\n{mue}\n")

        is_setup = self.__check_mods_setup(self)

        if is_setup:

            self.list = self.__get_list()

        else:

            self.list = self.mods_setup(self)

    @staticmethod
    def __check_mods_setup(self):

        is_setup = False

        if not os.path.isfile(self.config_file):
            print(f"File {self.config_file} not found, running setup.")
        else:
            is_setup = True

        return is_setup

    @staticmethod
    def mods_setup(self):

        if not os.path.exists('config'):
            os.makedirs('config')

        mod_name = 1

        while(True):

            print("Make sure you write in your moderator's username right\n\n")
            self.mod_username = str(input("Moderator username? (or 'q' to end) "))

            if self.mod_username == 'q':

                break

            try:

                user_data = self.mastodon.account_lookup(self.mod_username)

                user_role = self.mastodon.admin_account(user_data.id).role.name

                if user_role != '':

                    print(f"{self.mod_username}'s role is {user_role}, adding it")

                    self.list[f'{user_role}{str(mod_name)}'] = self.mod_username

                    mod_name += 1

                else:

                    print(f'{self.mod_username} is not a moderator or admin')

            except:
                print("account lookup error")
                pass

        if len(self.list) > 0:

            with open(self.config_file, 'wb') as the_file:
                print(f"Writing mods usernames to {self.config_file}")
                pickle.dump(self.list, the_file)

        return self.list

    def __get_list(self):

        if not os.path.isfile(self.config_file):
            print(f"File {self.config_file} not found..")

        with open(self.config_file, 'rb') as f:
            self.list = pickle.load(f)
            return self.list
